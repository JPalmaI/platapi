from fastapi import FastAPI
from fastapi.testclient import TestClient
from platapi.main import app


client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"msg":"It Works! :D"}