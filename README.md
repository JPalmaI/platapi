# platAPI
Proyecto realizado para estudio de la libreria FastAPI + Python. Inteta simular un completo entorno en donde se simula manejo de usuarios y dispositivos.

## Uso
Realiza peticiones HTTP a las direccion de la API.

- Ingresar a [localhost:8000/docs]() para ver la documentación de la API.
- Direcciones como `/token` devuelven un JWT para futuras peticiones.
- `/users/me` devuelven toda la informacion del usuario actual
- etc

# Requisitos
Estos paquetes son requisito para que la API funcione correctamente.
`
### Debian/Linux Mint

    sudo apt install mariadb-server mariadb-client libmariadb3 libmariadb-dev python3-dev python3-venv

### Variables de entorno
Configura un archivo `.env` con las siguientes Variables
    
    DB_CONNECT_STRING="mariadb+mysqlconnector://user:password@host/database"

# Instalación
Clona este repositorio y crea un entorno virtual dentro de la carpeta del respositorio. Luego de eso realiza la instalacion de los modulos necesarios.

### modulos de python

        git clone https://gitlab.com/JPalmaI/platapi
        cd platapi
        python3 -m venv .
        source bin/activate
        pip install -r requeriments.txt

### Migraciones
Dentro del entorno virtual hacer las migraciones utilizando alembic.

        alembic upgrade head

## Ejecutar la aplicación
Despues de haber instalado los requerimientos dentro del entorno virtual

    uvicorn --reload main:app

## Autor

** Josafat Palma **
- GitHub: [Github](https://github.com/jpalma26)
- LinkedIn: [LinkedIn](https://www.linkedin.com/in/josafat-palma-a15a68106/)