from fastapi import APIRouter,Depends
from fastapi.responses import JSONResponse
from typing import Union,Annotated
from platapi.dependencies import get_db
from sqlalchemy.orm import Session
from platapi.routers.login import get_current_active_user
from platapi.database.schemas import User,DashboardBase,DashboardUpdate,DashboardDelete
from platapi.database.crud.dashboard_crud import create_dashboard,update_dashboard as update_dashboardDB
from platapi.database.crud.dashboard_crud import delete_dashboard as delete_dashboardDB

router = APIRouter(
    prefix="/dashboards",
    tags=["dashboards"],
)

@router.post('/new',status_code=201)
def create_a_new_dashboard(current_user: Annotated[User, Depends(get_current_active_user)], 
        dashboard: DashboardBase,
        db: Session = Depends(get_db)
    ):
    result = create_dashboard(db,current_user,dashboard)
    if result:
        return JSONResponse(content='Dispositivo creado correctamente',status_code=201)
    else:
        return JSONResponse(content='No se pudo crear el dispositivo',status_code=400)

@router.post('/update')
def update_dashboard(current_user: Annotated[User, Depends(get_current_active_user)], 
        dashboard: DashboardUpdate,
        db: Session = Depends(get_db),
        ):

    updated_dashboard = update_dashboardDB(db,current_user,dashboard)

    if updated_dashboard:
        return JSONResponse(content='Dispositivo actualizado correctamente',status_code=200)
    else:
        return JSONResponse(content='No se pudo actualizar el dispositivo',status_code=400)

@router.post('/delete')
def delete_dashboard(current_user: Annotated[User, Depends(get_current_active_user)], 
        device: DashboardDelete,
        db: Session = Depends(get_db)):

    is_deleted = delete_dashboardDB(db,current_user,device.id)

    if is_deleted:
        return JSONResponse(content='Dispositivo eliminado correctamente',status_code=200)
    else:
        return JSONResponse(content='No se pudo eliminar el dispositivo',status_code=400)

@router.get('/all')
def get_all_dashboards(current_user: Annotated[User, Depends(get_current_active_user)]):
    current_dashboards = current_user.dashboards
    return current_dashboards