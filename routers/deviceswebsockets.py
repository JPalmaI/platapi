from fastapi import WebSocket, APIRouter, WebSocketDisconnect, Depends, HTTPException
from websockets.exceptions import ConnectionClosed
from typing import Set
from sqlalchemy.orm import Session
from platapi.dependencies import get_db
from platapi.routers.login import get_current_user
import asyncio
import time
import random as ra

router = APIRouter(
    prefix="/ws",
    tags=["websockets"],
)

# WebSocket para enviar datos en tiempo real
@router.websocket("/devices")
async def websocket_endpoint(websocket: WebSocket,
                            db: Session = Depends(get_db)):
    await websocket.accept()
    active_devices: Set[asyncio.Task[None]] = set()
    is_authenticated = False
    user = None
    try:
        """ 
        Websockets no tiene descrito como manejar la autenticación,
        siguiendo las guias de https://websockets.readthedocs.io/en/stable/topics/authentication.html
        utilizaré el primer mensaje en una nueva conexion para autenticar al usuario.
        """
        while True:
            #Recibimos el mensaje del cliente
            data = await websocket.receive_text()

            if not is_authenticated:
                is_authenticated, user = await verify_user(data,db)
                if is_authenticated:
                    print(user)
                    await websocket.send_text("Authenticated, Please send the device name.")
                    continue
                else:
                    await websocket.send_text("Not Authenticated, Bye")
                    continue

            if data == 'disconnect':
                await websocket.close(reason="Bye")
                return

            if "stop:" in data:
                device_name = data.replace('stop:','')
                for device_task in active_devices:
                    if device_name == device_task.get_name():
                        device_task.cancel()
                        await websocket.send_text(f"Stopped sending data for device {device_name}")
                    else:
                        await websocket.send_text(f"No active task found for device {device_name}")
            else:
                task = asyncio.create_task(send_data(data,websocket),name=data)
                active_devices.add(task)
                task.add_done_callback(active_devices.discard)

    except WebSocketDisconnect:
        """When a client disconnects, the await websocket.receive_text() method raises a WebSocketDisconnect exception """
        print("WebSocketDisconnect")
    except ConnectionClosed:
        print("ConnectionClosed")
    except HTTPException:
        await websocket.send_text("Not Authenticated, Bye")
        await websocket.close(reason="Not Authenticated")
        return

async def send_data(device: str, websocket: WebSocket):
    try:
        while True:
            temperature = ra.randint(20, 40)
            timestamp = time.strftime("%H:%M:%S")
            await websocket.send_json({"timestamp": timestamp, "temperature": temperature, "device": device})
            await asyncio.sleep(1)  # Envía datos cada 1 segundo
    except RuntimeError: #cuando el cliente se desconecta y la tarea aun intenta seguir enviando datos.
        return

async def verify_user(data: str, db):
    try:
        user = await get_current_user(data, db)
        return True, user
    except HTTPException:
        return False, None