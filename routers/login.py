from jose import JWTError, jwt
from dotenv import load_dotenv
import os
from datetime import timedelta,datetime
from typing import Annotated
from fastapi import Depends, HTTPException, status, APIRouter, Response
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from platapi.security.auth_handler import authenticate_user
from platapi.database.crud.user_crud import get_user_by_email
from platapi.database.schemas import User,Token, TokenData,UserCreate
from platapi.dependencies import get_db

load_dotenv()

router = APIRouter(
    prefix="/token",
    tags=["login"],
    responses={404: {"description": "Not found"}},
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
SECRET_KEY = os.getenv('SECRET_KEY','')
ALGORITHM = 'HS256'
ACCESS_TOKEN_EXPIRE_MINUTES = 60

def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=60)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(claims=to_encode, key=SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt

async def get_current_user(
    token: Annotated[str, Depends(oauth2_scheme)],
    db: Session = Depends(get_db)
):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token=token, key=SECRET_KEY, algorithms=ALGORITHM)
        username: str = payload.get("sub",'')
        email: str = payload.get("email",'')
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username,email=email)
    except JWTError:
        raise credentials_exception
    user = get_user_by_email(db, email=token_data.email)
    if user is None:
        raise credentials_exception
    return User.model_validate(user)


async def get_current_active_user(
    current_user: Annotated[User, Depends(get_current_user)]
):
    if not current_user.is_active:
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user

@router.post("", response_model=Token)
async def login_for_access_token(
    response: Response,
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    db: Session = Depends(get_db),
):
    credentials_exception = HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
    )
    user = get_user_by_email(db,form_data.username)
    if not user:
        raise credentials_exception
    is_authenticated = authenticate_user(form_data.username,form_data.password,db)
    if not is_authenticated:
        raise credentials_exception
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    payload = {
        'email': user.email,
        'sub': user.name,
        'lastname' : user.last_name
    }
    access_token = create_access_token(
        data=payload, expires_delta=access_token_expires
    )
    return {"access_token": access_token}

@router.get("/whoami")
def whoami( current_user: Annotated[User, Depends(get_current_active_user)], response: Response ):
    return current_user.email