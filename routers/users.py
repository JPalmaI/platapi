from fastapi import APIRouter, Depends, status, HTTPException
from typing import Union
from fastapi.responses import JSONResponse
from platapi.database.schemas import UserCreate
from platapi.database.crud.user_crud import get_user_by_email,create_user as create_userDB
from platapi.dependencies import get_db
from platapi.security.auth_handler import get_password_hash
from sqlalchemy.orm import Session
import bleach

HTTP_409_DETAIL = "Email ya registrado"
HTTP_400_DETAIL = "Nombre de usuario no permitido"
HTTP_201_DETAIL = "Usuario creado correctamente"

router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={
        201: {
            "description":"",
            "content":{
                "application/json":{
                    "example":{
                        "message": "Usuario Creado correctamente",
                    }
                },
            },
        },
        400:{
            "description":"",
            "content":{
                "application/json":{
                    "example":{
                        "message": "Usuario ya existe",
                    }
                },
            },
        }
    }
)

@router.post("/",status_code=201)
def create_user(user: UserCreate, db : Session = Depends(get_db)):

    # Verificar si el email ya existe
    existing_email = get_user_by_email(db, sanitized_email)
    if existing_email:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail=HTTP_409_DETAIL
        )
    
    sanitized_name = bleach.clean(user.name)
    sanitized_last_name = bleach.clean(user.last_name) if user.last_name else None
    sanitized_email = bleach.clean(user.email)
    hashed_password = get_password_hash(user.password)

    validated_user = UserCreate.model_validate({
        "name": sanitized_name,
        "last_name": sanitized_last_name,
        "email" : sanitized_email,
        "password": hashed_password
    })

    user_created = create_userDB(db,validated_user)
    
    return JSONResponse(
            content=HTTP_201_DETAIL,
            status_code=status.HTTP_201_CREATED
        )
    
