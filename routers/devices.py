from fastapi import APIRouter, Depends, status, HTTPException, Body
from typing import Union,Annotated
from fastapi.responses import JSONResponse
from platapi.database.schemas import DeviceCreate,User,Device,DeviceUpdate,DeviceBase,DeviceDelete
from platapi.database.crud.device_crud import get_device_by_id,create_device as create_deviceDB,update_device,get_devices,delete_device as delete_deviceDB
from platapi.dependencies import get_db
from sqlalchemy.orm import Session
from platapi.routers.login import get_current_active_user

router = APIRouter(
    prefix="/devices",
    tags=["devices"],
)

@router.post('/new',status_code=201)
def create_new_device(current_user: Annotated[User, Depends(get_current_active_user)], 
        device: DeviceBase,
        db: Session = Depends(get_db)
    ):
    result = create_deviceDB(db,current_user,device)
    if result:
        return JSONResponse(content='Dispositivo creado correctamente',status_code=201)
    else:
        return JSONResponse(content='No se pudo crear el dispositivo',status_code=400)

@router.post('/update')
def update_device_by_id(current_user: Annotated[User, Depends(get_current_active_user)], 
        device: DeviceUpdate,
        db: Session = Depends(get_db),
        ):

    updated_device = update_device(db,current_user,device)

    if updated_device:
        return JSONResponse(content='Dispositivo actualizado correctamente',status_code=200)
    else:
        return JSONResponse(content='No se pudo actualizar el dispositivo',status_code=400)

@router.post('/delete')
def delete_device(current_user: Annotated[User, Depends(get_current_active_user)], 
        device: DeviceDelete,
        db: Session = Depends(get_db)):

    deleted_device = delete_deviceDB(db,current_user,device.id)

    if deleted_device:
        return JSONResponse(content='Dispositivo eliminado correctamente',status_code=200)
    else:
        return JSONResponse(content='No se pudo eliminar el dispositivo',status_code=400)

@router.get('/all')
def get_all_devices(current_user: Annotated[User, Depends(get_current_active_user)]):
    current_devices = current_user.devices
    return current_devices

