from pydantic import BaseModel, EmailStr, field_validator, ConfigDict

""" 
    Devices
"""
class DeviceBase(BaseModel):
    name: str
    device_data_type: str = 'temp'

    @field_validator('name')
    @classmethod
    def validate_name(cls,v :str) -> str:
        if v.isspace():
            raise ValueError('Nombre invalido')
        return v.strip()

    @field_validator('device_data_type')
    @classmethod
    def validate_prot(cls,v :str) -> str:
        if v not in ['temp','hum']:
            raise ValueError('Tipo de dato invalido')
        return v

    @field_validator('name','device_data_type')
    @classmethod
    def validate_prop(cls,value: str) -> str:
        if value == '':
            raise ValueError('No se puede crear un dispositivo sin nombre/tipo dato')
        return value

class DeviceCreate(DeviceBase):
    user_id: int

class DeviceUpdate(DeviceBase):
    id: int
    model_config = ConfigDict(from_attributes=True)

        
class DeviceDelete(BaseModel):
    id: int

class Device(DeviceBase):
    id: int
    
    model_config = ConfigDict(from_attributes=True)

""" 
    Dashboard
"""
class DashboardBase(BaseModel):
    name: str
    description: str | None = None
    gridstack_layout: str | None = "{}"
    
    @field_validator('name')
    @classmethod
    def validate_name(cls,v :str) -> str:
        if v is None:
            raise ValueError('Nombre Invalido')
        if v.isspace() or v == '':
            raise ValueError('Nombre Invalido')
        return v.strip()

    @field_validator('description')
    @classmethod
    def validate_desc(cls,v :str) -> str:
        if v is None:
            return ''
        if v.isspace():
            return None
        return v.strip()


class DashboardCreate(DashboardBase):
    user_id: int
    pass

class DashboardUpdate(DashboardBase):
    id: int
    model_config = ConfigDict(from_attributes=True)


class DashboardDelete(BaseModel):
    id: int

class Dashboard(DashboardBase):
    id: int
    user_id: int

    model_config = ConfigDict(from_attributes=True)


""" 
    User
"""
class UserBase(BaseModel):
    email: EmailStr
    name: str
    last_name: str | None = None

    @field_validator("name")
    def validate_name(cls, v):
        # Permitir solo letras y espacios (para nombres como "María José")
        allowed_chars = set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZáéíóúÁÉÍÓÚñÑ ")
        if not all(c in allowed_chars for c in v):
            raise ValueError("El nombre solo puede contener letras y espacios")
        return v

    @field_validator("last_name")
    def validate_last_name(cls, v):
        if v is None:  # Ignorar si last_name es None
            return v
        # Permitir solo caracteres alfanuméricos (sin espacios ni caracteres especiales)
        if not v.isalnum():
            raise ValueError("El apellido solo puede contener caracteres alfanuméricos")
        return v

    @field_validator("name")
    def validate_suspicious_names(cls, v):
        suspicious_names = ["admin", "root", "system"]
        if any(name in v.lower() for name in suspicious_names):
            raise ValueError("Nombre no permitido")
        return v
    
class UserCreate(UserBase):
    password: str

    @field_validator("password")
    def validate_password(cls, v):
        if len(v) < 8:
            raise ValueError("La contraseña debe tener al menos 8 caracteres")
        if not any(c.isupper() for c in v):
            raise ValueError("La contraseña debe contener al menos una mayúscula")
        if not any(c.isdigit() for c in v):
            raise ValueError("La contraseña debe contener al menos un número")
        return v

class User(UserBase):
    id: int
    is_active: bool = True
    dashboards: list[Dashboard] = []
    devices: list[Device] = []

    model_config = ConfigDict(from_attributes=True)


""" 
    JWT Model
"""

class Token(BaseModel):
    access_token: str
    token_type: str = 'bearer'


class TokenData(BaseModel):
    username: str | None = None
    email: EmailStr