from .dbconn import Base
from sqlalchemy import ForeignKey, String, Text
from sqlalchemy.dialects.mysql import JSON
from sqlalchemy.orm import relationship, Mapped, mapped_column
from typing import List, Optional

class User(Base):
    __tablename__ = "users"
    id : Mapped[int] = mapped_column(primary_key=True,index=True,unique=True)
    is_active : Mapped[bool] = mapped_column(server_default="1")
    email : Mapped[str] = mapped_column(String(255),unique=True, index=True)
    password : Mapped[str] = mapped_column(String(255))
    name : Mapped[str] = mapped_column(String(255))
    last_name : Mapped[Optional[str]] = mapped_column(String(255))

    """ Esta propiedad de la clase no se genera como una columna en la DB,
    en cambio es populada por SQLAlchemy para facilitar el uso de las relaciones
    se accede como User.devices y ahi se hace la peticion a la otra tabla ubicada en la clase Device y Dashboard
    """
    dashboards: Mapped[List["Dashboard"]] = relationship()
    devices: Mapped[List["Device"]] = relationship()

class Dashboard(Base):
    __tablename__ = "dashboards"

    id : Mapped[int] = mapped_column(primary_key=True,index=True,unique=True)
    name : Mapped[str] = mapped_column(String(255))
    description: Mapped[Optional[str]] = mapped_column(Text())
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))
    gridstack_layout: Mapped[dict] = mapped_column(JSON, nullable=False, server_default="{}")

class Device(Base):
    __tablename__ = "devices"

    id : Mapped[int] = mapped_column(primary_key=True,index=True,unique=True)
    name : Mapped[str] = mapped_column(String(255))
    device_data_type : Mapped[str] = mapped_column(String(4),server_default='temp')
    user_id: Mapped[int] = mapped_column(ForeignKey("users.id"))

#parent_id = db.Column(db.Integer, db.ForeignKey('parent.id', ondelete='CASCADE'))
#parent = db.relationship('Parent', backref=backref('children', passive_deletes=True))