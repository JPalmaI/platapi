from sqlalchemy.orm import Session
from sqlalchemy import update as sqlalchemy_update
from platapi.database.schemas import UserCreate,User as UserSchema
from platapi.database.models import User as UserModel

def get_user_by_id(db: Session, user_id: int):
    return db.query(UserModel).filter(UserModel.id == user_id).first()

def get_user_by_email(db: Session, email: str):
    return db.query(UserModel).filter(UserModel.email == email).first()

def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(UserModel).offset(skip).limit(limit).all()

def create_user(db: Session, user: UserCreate):
    db_user = UserModel(**user.model_dump())
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def update_user(db: Session, user: UserSchema):
    user_dict = user.model_dump(exclude={'dashboards','hashed_password','id'})
    query = sqlalchemy_update(UserModel).where(
            UserModel.id == user.id).values(**user_dict)
    db.execute(query)
    db.commit()
    return

def change_password(db: Session, user : UserSchema, new_password: str):
    db_user = get_user_by_id(db=db,user_id=user.id)
    db_user.hashed_password = new_password
    db.commit()
    db.refresh(db_user)
    return

def delete_user_by_id(db: Session, user_id: int):
    db_user = get_user_by_id(db=db,user_id=user_id)
    db.delete(db_user)
    db.commit()
    return
