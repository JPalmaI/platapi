from sqlalchemy.orm import Session
from sqlalchemy import update as sqlalchemy_update
from sqlalchemy.exc import SQLAlchemyError
from platapi.database.models import Dashboard as DashboardModel
from platapi.database.schemas import DashboardCreate,Dashboard as DashboardSchema,User,DashboardBase,DashboardCreate
from platapi.database.schemas import DashboardUpdate

def get_dashboard_by_id(db: Session, user_id: int, dashboard_id: int):
    return db.query(DashboardModel).filter(DashboardModel.id == dashboard_id,DashboardModel.user_id == user_id).first()

def get_dashboards(db: Session, user: User, skip: int = 0, limit: int = 100):
    return db.query(DashboardModel).filter(DashboardModel.user_id == user.id).offset(skip).limit(limit).all()

def create_dashboard(db: Session, user: User, dashboard: DashboardBase):
    try:
        db_dashboard = DashboardModel(**dashboard.model_dump(),user_id=user.id)
        db.add(db_dashboard)
        db.commit()
        db.refresh(db_dashboard)
        return db_dashboard
    except SQLAlchemyError:
        db.rollback()
        return False


def update_dashboard(db: Session, user: User, dashboard: DashboardUpdate):
    result = 0
    try:
        device_dict = dashboard.model_dump()
        query = sqlalchemy_update(DashboardModel).where(
                DashboardModel.id == dashboard.id,DashboardModel.user_id == user.id).values(**device_dict)
        result_db = db.execute(query)
        db.commit()
        result = result_db.rowcount
    except SQLAlchemyError as e:
        db.rollback()
    
    if result > 0:
        return True
    else:
        return False

def delete_dashboard(db: Session, user: User, dashboard_id : int):
    db_dashboard = get_dashboard_by_id(db=db,dashboard_id=dashboard_id,user_id=user.id)
    
    if db_dashboard is not None:
        db.delete(db_dashboard)
        db.commit()
        return True
    else:
        return False
