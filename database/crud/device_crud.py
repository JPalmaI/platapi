from sqlalchemy.orm import Session,deferred
from sqlalchemy import update as sqlalchemy_update
from sqlalchemy.exc import SQLAlchemyError
from platapi.database.models import Device as DeviceModel
from platapi.database.schemas import DeviceCreate,Device as DeviceSchema, DeviceUpdate,DeviceBase,User

def get_device_by_id(db: Session, user_id: int, device_id: int):
    return db.query(DeviceModel).filter(DeviceModel.id == device_id,DeviceModel.user_id == user_id).first()

def get_devices(db: Session, user: User, skip: int = 0, limit: int = 100 ):
    return db.query(DeviceModel).filter(DeviceModel.user_id == user.id).offset(skip).limit(limit).all()

def create_device(db: Session, user: User, device: DeviceBase):
    try:
        db_device = DeviceModel(**device.model_dump(),user_id=user.id)
        db.add(db_device)
        db.commit()
        db.refresh(db_device)
        return db_device
    except SQLAlchemyError as e:
        db.rollback()
        return False

def update_device(db: Session, user: User, device: DeviceUpdate):
    result = 0
    try:
        device_dict = device.model_dump()
        query = sqlalchemy_update(DeviceModel).where(
                DeviceModel.id == device.id,DeviceModel.user_id == user.id).values(**device_dict)
        result_db = db.execute(query)
        db.commit()
        result = result_db.rowcount
    except SQLAlchemyError as e:
        db.rollback()
    
    if result > 0:
        return True
    else:
        return False

def delete_device(db: Session, user: User, device_id : int):
    db_device = get_device_by_id(db=db,device_id=device_id,user_id=user.id)
    
    if db_device is not None:
        db.delete(db_device)
        db.commit()
        return True
    else:
        return False
