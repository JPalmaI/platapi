"""Actualiza tipo de dato a JSON utilizando MariaDB 10.11

Revision ID: c06778af2397
Revises: 00be297335a8
Create Date: 2024-12-29 13:44:21.669978

"""
from typing import Sequence, Union

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.mysql import JSON

# revision identifiers, used by Alembic.
revision: str = 'c06778af2397'
down_revision: Union[str, None] = '00be297335a8'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    # Modificar el tipo de columna gridstack_layout
    with op.batch_alter_table('dashboards') as batch_op:
        batch_op.alter_column(
            'gridstack_layout',
            type_=JSON,  # Cambiar a JSON
            existing_type=sa.Text,  # Tipo anterior (LONGTEXT)
            nullable=False,
            existing_nullable=False,
            server_default='{}'
        )


def downgrade() -> None:
    # Revertir a LONGTEXT si se hace downgrade
    with op.batch_alter_table('dashboards') as batch_op:
        batch_op.alter_column(
            'gridstack_layout',
            type_=sa.Text,
            existing_type=JSON,
            nullable=False,
            existing_nullable=False
        )
