from sqlalchemy.orm import Session
from platapi.database.crud.user_crud import get_user_by_email
from argon2 import PasswordHasher

#contexto de crypt
ph = PasswordHasher()

def verify_password(plain_password: str, password: str):
    return ph.verify(password, plain_password) #hashed_password, plain_password

def get_password_hash(password: str):
    return ph.hash(password)

def authenticate_user(email: str, password: str,db: Session):
    user = get_user_by_email(db, email)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return True