from .routers import users, login, devices, dashboards, deviceswebsockets
from .dependencies import get_db
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()
app.include_router(users.router)
app.include_router(login.router)
app.include_router(devices.router)
app.include_router(dashboards.router)
app.include_router(deviceswebsockets.router)
origins = [
    "http://localhost:5173",
    "http://127.0.0.1:5173",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get('/')
def homepage():
    return {"msg":"It Works! :D"}